import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ex_parcial/const/consts.dart';
import 'package:ex_parcial/screens/menus_screen.dart';
import 'package:ex_parcial/screens/widgets/widgets_popup.dart';
import 'package:firebase/firebase.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class CrudScreen extends StatefulWidget {
  CrudScreen({Key key}) : super(key: key);

  @override
  _CrudScreenState createState() => _CrudScreenState();
}

class _CrudScreenState extends State<CrudScreen> {
  String id = "";
  String tittle = "";
  String description = "";
  String status = "";
  String positionId = "0";
  String fatherId = "";

  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();

    return firebaseApp;
  }

  @override
  void initState() {
    super.initState();
    _initializeFirebase;
  }

  getMenuName(name) {
    this.tittle = name;
  }

  getMenuId(id) {
    this.id = id;
  }

  getDescriptionID(descriptionID) {
    this.description = descriptionID;
  }

  getStatusID(statusID) {
    this.status = statusID;
  }

  getPositionID(positionID) {
    this.positionId = positionID;
  }

  getFatherID(fatherID) {
    this.fatherId = fatherID;
  }

  CollectionReference menuss = FirebaseFirestore.instance.collection('menus');
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  createData() {
    return menuss.add({
      "tittle": tittle,
      "description": description,
      "father_id": fatherId,
      "position": positionId,
      "status": status,
    }).then((value) {
      barAviso(context, "Menú agregado");
      print("User Added");
    }).catchError((error) => print("Failed to add user: $error"));
  }

  readData() {}

  Future<void> updateData() {
    print("updated");

    // Call the user's CollectionReference to add a new user
  }

  deleteData() {
    print("deleted");
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs
        .map(
          (doc) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 300),
            child: new ListTile(
              leading: Icon(Icons.title),
              title: new Text(doc["tittle"]),
              subtitle: new Text(doc["description"].toString()),
              trailing: Text(doc["status"].toString()),
            ),
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(50),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 8),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "tittle",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 2,
                      ),
                    ),
                  ),
                  onChanged: (String name) {
                    getMenuName(name);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 8),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "position id",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 2,
                      ),
                    ),
                  ),
                  onChanged: (String name) {
                    getPositionID(name);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 8),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "description id",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 2,
                      ),
                    ),
                  ),
                  onChanged: (String name) {
                    getDescriptionID(name);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 8),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "status id",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 2,
                      ),
                    ),
                  ),
                  onChanged: (String name) {
                    getStatusID(name);
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    onPressed: () {
                      createData();
                    },
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Text("Crear"),
                    textColor: Colors.white,
                  ),
                  RaisedButton(
                    onPressed: () {
                      readData();
                    },
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Text("Read"),
                    textColor: Colors.white,
                  ),
                  RaisedButton(
                    onPressed: updateData,
                    color: Colors.orange,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Text("Update"),
                    textColor: Colors.white,
                  ),
                  RaisedButton(
                    onPressed: () {
                      deleteData();
                    },
                    color: Colors.red,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Text("Delete"),
                    textColor: Colors.white,
                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              RaisedButton(
                onPressed: () {
                  agregarPopUp(context);
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => PlutoMenuBarDemo()));
                },
                child: Text("Agregar"),
                color: Colors.amber,
              ),
              StreamBuilder<QuerySnapshot>(
                stream: firestore.collection("menus").snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) return new Text("There is no expense");
                  return Expanded(
                      child: new ListView(children: getExpenseItems(snapshot)));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
