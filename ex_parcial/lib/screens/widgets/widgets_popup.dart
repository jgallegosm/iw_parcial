import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ex_parcial/const/consts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

String id = "";
String tittle = "";
String description = "";
String status = "";
String positionId = "0";
String fatherId = "";

Future<FirebaseApp> _initializeFirebase() async {
  FirebaseApp firebaseApp = await Firebase.initializeApp();

  return firebaseApp;
}

getMenuName(name) {
  tittle = name;
}

getMenuId(id) {
  id = id;
}

getDescriptionID(descriptionID) {
  description = descriptionID;
}

getStatusID(statusID) {
  status = statusID;
}

getPositionID(positionID) {
  positionId = positionID;
}

getFatherID(fatherID) {
  fatherId = fatherID;
}

CollectionReference menuss = FirebaseFirestore.instance.collection('menus');
CollectionReference users = FirebaseFirestore.instance.collection('users');
FirebaseFirestore firestore = FirebaseFirestore.instance;

createData(BuildContext context) {
  return menuss.add({
    "tittle": tittle,
    "description": description,
    "father_id": fatherId,
    "position": positionId,
    "status": status,
  }).then((value) {
    Navigator.pop(context);
    barAviso(context, "Menú agregado");
    print("User Added");
  }).catchError((error) => print("Failed to add user: $error"));
}

void agregarPopUp(BuildContext context) {
  showDialog(
    context: context,
    builder: (_) => AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(32.0),
        ),
      ),
      title: Center(child: Container(child: Text("Agregar tittulo?"))),
      content: Container(
        child: Scaffold(
          body: Padding(
            padding: EdgeInsets.all(50),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "tittle",
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 2,
                        ),
                      ),
                    ),
                    onChanged: (String name) {
                      getMenuName(name);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "position id",
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 2,
                        ),
                      ),
                    ),
                    onChanged: (String name) {
                      getPositionID(name);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "description id",
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 2,
                        ),
                      ),
                    ),
                    onChanged: (String name) {
                      getDescriptionID(name);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "status id",
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 2,
                        ),
                      ),
                    ),
                    onChanged: (String name) {
                      getStatusID(name);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () async {
                        await createData(context);
                        // Navigator.pop(context);
                      },
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Text("Crear"),
                      textColor: Colors.white,
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}
