// To parse this JSON data, do
//
//     final menu = menuFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

// Menu menuFromJson(String str) => Menu.fromJson(json.decode(str));

// String menuToJson(Menu data) => json.encode(data.toJson());

class Menu {
  Menu({
    this.tittle = "",
    this.description = "",
    this.fatherId = 0,
    this.status = false,
    this.positionId = 0,
  });

  String tittle;
  String description;
  int fatherId;
  bool status;
  int positionId;

  // factory Menu.fromJson(Map<String, dynamic> json) => Menu(
  //       tittle: json["tittle"],
  //       description: json["description"],
  //       fatherId: json["father_id"],
  //       status: json["status"],
  //       positionId: json["position_id"],
  //     );

  // Map<String, dynamic> toJson() => {
  //       "tittle": tittle,
  //       "description": description,
  //       "father_id": fatherId,
  //       "status": status,
  //       "position_id": positionId,
  //     };
}
