import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: Color(0xFF6CA8F1),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);
Widget barAviso(BuildContext context, String mensaje) {
  return Flushbar(
    flushbarPosition: FlushbarPosition.BOTTOM,
    message: mensaje,
    duration: Duration(seconds: 5),
  )..show(context);
}
